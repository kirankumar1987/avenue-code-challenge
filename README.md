# REST SERVICE challenge #

This project has been developed using spring boot which provided embedded tomcat instance with faster development and testing



Steps to run the project

1. checkout the project and run gradle build
2. import as gradle project
3. run MyChallengeApplication as java application ( just by right clicking). spirng boot starts embedded tomcat instance and published following services

A. ProductService
B. Order Service

How to Test ?

1. To retreive all products use the link below
    http://localhost:8080/products/all
   
2. To retrieve specific product

http://localhost:8080/products/1201


Testing OrderService

1. localhost:8080/orders/all  ( to retrieve all orders)

2. localhost:8080/orders ( PUT method to add order)

3. localhost:8080/orders/id ( POST method to update order)

# How to run test cases ?

Go to MyChallengeApplicationTests and right click on and run as junit test.


    

