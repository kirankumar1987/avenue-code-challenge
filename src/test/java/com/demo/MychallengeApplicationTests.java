package com.demo;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.demo.domain.Product;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@EnableAutoConfiguration
public class MychallengeApplicationTests {
	
	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void testProductService() throws JsonParseException, JsonMappingException, IOException {
		
		ResponseEntity<String> productsJson = this.restTemplate.getForEntity("/products/all", String.class);

		ObjectMapper objectMapper = new ObjectMapper();
		List<Product> products = objectMapper.readValue(productsJson.getBody(), new TypeReference<List<Product>>(){});

		assertEquals("1201", products.get(0).getId());
	}
	
	
	@Test
	public void testProductById() throws MalformedURLException {
		
		Product product = this.restTemplate.getForObject("/products/1201", Product.class);
		System.out.println("product id: "+product.getId());

		assertEquals("1201", product.getId());
	}

}
