package com.demo.service;

import java.util.Collection;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.demo.domain.Order;

@RestController
@RequestMapping("/orders")
public class OrderController {
	
	@Autowired
	private OrderService orderService;
	
	
	@RequestMapping(method=RequestMethod.PUT, consumes="application/json")
	public void addOrder(@RequestBody Order order) {
		System.out.println("Order is added "+order.toString());
		orderService.addOrder(order);
		
	}
	
	@RequestMapping(method=RequestMethod.POST, value = "/{id}", consumes="application/json")
	public void updateOrder(@RequestBody Order order, @PathVariable String id) {
		System.out.println("Order is updated for id "+order.getId());
		orderService.updateOrder(order, Integer.valueOf(id));
		
	}
	
	@RequestMapping(method=RequestMethod.GET, value = "/{id}", produces = "application/json")
	public Order getOrder(@PathVariable String id) {
		System.out.println("Retrieving Order for id "+id);
		Order order =orderService.getOrder(id);
		return order;
		
	}
	
	@RequestMapping("/all")
	public Collection<Order> getAllOrders() {		
		return orderService.retrieveAllOrders();
		
	}
	

}
