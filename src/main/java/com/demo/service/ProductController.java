package com.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.demo.domain.Product;

@RestController
@RequestMapping("/products")
public class ProductController {		
	
	@Autowired
	ProductService productService;
	
	@RequestMapping("/all")
	public List<Product> getAllProducts() {		
		return productService.retrieveAllProducts();
		
	}
	

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", produces = "application/json")
	public Product getproductbyId(@PathVariable String id) {		
		return productService.retriveProductInfo(id);
		
	}


}
