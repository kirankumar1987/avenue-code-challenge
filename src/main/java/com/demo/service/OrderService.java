package com.demo.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.demo.domain.Order;
import com.demo.domain.Product;

@Component
public class OrderService {
	
	@Autowired
	OrderCache orderCache;
	
	/**
	 * 
	 * Get all orders
	 */
	
	public Collection<Order> retrieveAllOrders() {
		return orderCache.getAllOrders();
	}
	
	public void addOrder (Order order) {
		order.setCreatedDate(Calendar.getInstance().getTime());
		orderCache.addOrders(order);
	}
	
	public void updateOrder(Order order, int id) {
		if(orderCache.getOrder(order.getId()) != null) {
			order.setUpdatedDate(Calendar.getInstance().getTime());
			orderCache.updateOrder(order,id);
		}
	}

	public Order getOrder(String id) {
		return orderCache.getOrder(Integer.valueOf(id));
		
	}


}
