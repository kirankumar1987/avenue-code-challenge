package com.demo.service;

import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.demo.domain.Order;

public class OrderCache {
	
	private  static Map<Integer,Order> orders = new HashMap<Integer,Order>();
	
	public OrderCache() {
		orders.put(100, new Order(100,"medical order", "Kiran", Calendar.getInstance().getTime()));
	}
	
	public void addOrders(Order order){
		orders.put(order.getId(), order);
		
	}
	
	public Order getOrder(Integer orderId) {
		return orders.get(orderId);
	}
	
	public void updateOrder(Order order,Integer id) {
		orders.put(id, order);
	}
	
	public Collection<Order> getAllOrders() {
		return orders.values();
	}

}
