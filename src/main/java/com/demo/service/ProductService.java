package com.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.demo.domain.Product;

@Component
public class ProductService {
	
	@Autowired
	ProductCache productCache;
	
	public List<Product> retrieveAllProducts() {
		List<Product> products = new ArrayList<Product>();
		products.add(new Product("1201", "Dell latitude Laptop", 1389.0));
		return products;
	}
	
	public Product retriveProductInfo(String productId) {
		return productCache.getDesiredProduct(productId);
	}

}
