package com.demo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.demo.service.OrderCache;
import com.demo.service.ProductCache;

@Configuration
public class Config {
	
	@Bean
	public ProductCache productCache (){
		return new ProductCache();
	}
	
	@Bean
	public OrderCache orderCache (){
		return new OrderCache();
	}

}
