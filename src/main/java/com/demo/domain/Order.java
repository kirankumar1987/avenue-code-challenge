package com.demo.domain;

import java.util.Date;

import javax.validation.constraints.NotNull;

import com.demo.util.JsonDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class Order {
	
	private int id;
	private String description;
	
	//@NotNull
	private String purchaser;
	private Date createdDate;
	private Date updatedDate;
	
	public Order(){
		
	}
	
	public Order(Integer id, String description, String purchaser, Date createdDate) {
		this.id = id;
		this.description = description;
		this.purchaser = purchaser;
		this.createdDate = createdDate;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@JsonSerialize(using=JsonDateSerializer.class)
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date placedDate) {
		this.createdDate = placedDate;
	}

	public String getPurchaser() {
		return purchaser;
	}

	public void setPurchaser(String purchaser) {
		this.purchaser = purchaser;
	}

	@JsonSerialize(using=JsonDateSerializer.class)
	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	

}
